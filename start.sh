#!/bin/bash

read -p "Are you sure that you are in the directory you want to be in to start a wordpress project? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Navigate to the directory that you want to start in and try again."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


read -p "Do you have a local empty database for this project ^AND^ you use root:root@127.0.0.1:3306 to connect? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Create an empty local dev database for this project and try again."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


read -p "What is the name of this database? " -r
echo
if [[ ! $REPLY =~ ^[0-9a-zA-Z\$_]+$ ]]
then
    echo "The database name you entered can not possibly be correct. Please rerun this script and type in a valid correct database name!"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

DATABASE=$REPLY


read -p "Do you have a local hostname for this project? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Create a local hostname for this project and try again."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


read -p "What is the local hostname of this project? " -r
echo
if [[ ! $REPLY =~ ^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$ ]]
then
    echo "The host name that you entered does not match a valid RFC 1123 approved hostname."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi
HOSTNAME=$REPLY


# First delete everything in the directory
# rm -rf ./*

# OK go get the repo as a zipo.
wget -q https://gitlab.com/ccpub/wp-start/-/archive/master/wp-start-master.zip
unzip wp-start-master.zip
rm -rf wp-start-master.zip
dd='wp-start-master'
echo $dd
mv $dd/* ./
mv $dd/.gitignore ./
mv $dd/.htaccess ./
rm -rf $dd
rm start.sh
rm README.md

# isntall grumpers
composer install

# make the project theme
cd wp-content/themes/project
npm install
npm run build
cd -

#now the wordpress
wp core download
cp wp-config-sample.php wp-config.php
wp config set DB_NAME $DATABASE
wp config set DB_USER root
wp config set DB_PASSWORD root
wp config set DB_HOST 127.0.0.1
wp config shuffle-salts
wp config set WP_DEBUG true --raw
wp config set WP_HOME https://$HOSTNAME
wp config set WP_SITEURL https://$HOSTNAME
wp config set table_prefix $DATABASE\_


wp db reset --yes
wp core install --url=https://$HOSTNAME --title=$DATABASE --admin_user=$DATABASE --admin_email=$DATABASE@$DATABASE.com

# Install plugins
sh wp-content/plugins/plugins.sh local

wp plugin activate ccfigs
wp plugin activate ccforms
wp plugin activate project

# Activate the theme
wp theme activate project

# Timezone
wp option update timezone_string Europe/Brussels
# Files folders
wp option set uploads_use_yearmonth_folders 0


# Image Sizes

wp option set medium_large_size_w 0
wp option set medium_large_size_h 0
wp option set large_size_h 0
wp option set large_size_w 0
wp option set medium_size_w 0
wp option set medium_size_h 0
wp option set thumbnail_size_h 150
wp option set thumbnail_size_w 150
wp option set thumbnail_crop 1

# TinyMCE Advanced

wp db query "INSERT INTO ${DATABASE}_options (option_name, option_value, autoload) VALUES ('tadv_settings', 'a:10:{s:9:\"toolbar_1\";s:132:\"formatselect,undo,redo,pastetext,removeformat,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,link,unlink,table,charmap\";s:9:\"toolbar_2\";s:0:\"\";s:9:\"toolbar_3\";s:0:\"\";s:9:\"toolbar_4\";s:0:\"\";s:21:\"toolbar_classic_block\";s:154:\"formatselect,undo,redo,pastetext,removeformat,bold,italic,subscript,superscript,bullist,numlist,alignleft,aligncenter,link,unlink,table,blockquote,charmap\";s:13:\"toolbar_block\";s:67:\"core/code,core/image,core/strikethrough,tadv/mark,tadv/removeformat\";s:18:\"toolbar_block_side\";s:46:\"core/superscript,core/subscript,core/underline\";s:12:\"panels_block\";s:0:\"\";s:7:\"options\";s:34:\"merge_toolbars,advlist,contextmenu\";s:7:\"plugins\";s:25:\"table,advlist,contextmenu\";}', 'yes')"

wp db query "INSERT INTO ${DATABASE}_options (option_name, option_value, autoload) VALUES ('tadv_admin_settings', 'a:2:{s:7:\"options\";s:44:\"table_grid,table_tab_navigation,table_advtab\";s:16:\"disabled_editors\";s:0:\"\";}', 'yes')"

wp db query "INSERT INTO ${DATABASE}_options (option_name, option_value, autoload) VALUES ('tadv_version', '5500', 'yes')"


wp rewrite structure '/%postname%/'
wp rewrite flush

wp user update 1 --user_pass=$DATABASE --display_name=$DATABASE --role='administrator'
