# Run This
```
bash <(wget -qO- https://gitlab.com/ccpub/wp-start/-/raw/master/start.sh)
```

## When it is done

- Login into the admin with the following
	- $DATABASE / $DATABASE

*Where $DATABASE is the name of the database that you installed this to.*

### Cleanup the Dashboard

- Hide everything from screen options in the dashboard

### Polylang

- Create the languages that you need in the polylang languages
- Configure polylang it self to show the default language in the url
- Configure polylang to not translate media

### Comments

- Configure the disable comments plugin to not allow any comments

### Comments

- Complete the redirection setup

### Settings General
- Change the site title
- Change the site tagline
- Change Administration Email Address
- Change the date format d/m/Y
- Change the time format H:i

### Create a Home page

### Create a Posts page

### Publish the Privacy Page

### Delete Sample Pages and Sample Posts

### Settings Reading

- Change Your Home Page
- Change Your Posts page

### Settings Privacy

- Set the privacy page

### Settings Advanced Editor Tools

- strip down tinymce in all places to the bare minimum needed
- disable setting text color
- disable setting background color
- disable editor menu
- remove text color from editor
- remove background color from editor

### Image Sizes

- Disable all except thumbnail

### Media

- Do not organize uploads into folders

# Run This To Get The Plugins 
```
bash <(wget -qO- https://gitlab.com/ccpub/wp-start/-/raw/master/wp-content/plugins/plugins.sh) local
```

or

```
sh wp-content/plugins/plugins.sh local
```
