<div class="container-md">
    <div class="row lazyblock-partners">
        <?php
        $args = [
          'post_type'      => 'partners',
          'posts_per_page' => -1,
          'order'          => 'ASC',
          'orderby'        => 'title',
        ];

        // Maybe it's multilingual
        if (function_exists('pll_current_language')) {
            $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
        }

        // Custom query.
        $myquery = new WP_Query($args);
        // Check that we have query results.
        if ($myquery->have_posts()) {
            ?>
        <div class="row partners-wrapper">
            <?php
            // Start looping over the query results.
            while ($myquery->have_posts()) {
                $myquery->the_post();
                $link = get_field('link');
                ?>
                <div class="col-lg-3 col-md-6 col-12 p-3 partner">
                    <div class="partner-inner p-3">
                        <div class="col-12 text-center partner-image mt-3 mb-3"><img src="<?= get_field('image') ?>" /></div>
                        <div class="col-12 text-center mb-4"><?= the_title() ?></div>
                        <div class="col-12 text-center mt-2 mb-1 partner-link"><a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" class="btn btn-blue"><?= $link['title'] ?></a></div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>
    </div>

</div>