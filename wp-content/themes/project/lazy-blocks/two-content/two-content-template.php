<div class="row lazyblock-two-content">
  <div class="col-lg-{{division}} {{#if contentoneblue}}blue-box{{/if}}">
    {{{contentone}}}
  </div>
  <div class="col-lg-{{#leftover division}} {{#if contenttwoblue}}blue-box{{/if}}">
    {{{contenttwo}}}
  </div>
</div>
