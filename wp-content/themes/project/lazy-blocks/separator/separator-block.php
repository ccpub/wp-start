<?php

$blockslug = 'separator';

// Start the block
$block             = [];
$block['title']    = 'Separator';
$block['icon']     = 'dashicons dashicons-image-flip-vertical';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Text
$control                         = [];
$control['label']                = 'Height (0-1000)';
$control['name']                 = 'height';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min'] = '0';
$control['max'] = '1000';
$control['step'] = '5';
$control['default'] = '10';

// Make an id.
$control_id = 'control-separator-height';

// Add the control to the controls
$controls[$control_id] = $control;

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
