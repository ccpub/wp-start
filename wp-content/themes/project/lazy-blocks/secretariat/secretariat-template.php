<div class="container secretariat-container">
    <div class="row lazyblock-secretariat justify-content-around">
        <?php
        $args = [
          'post_type'      => 'secretariat_item',
          'posts_per_page' => -1,
          'meta_key' => 'order',
          'orderby' => 'meta_value_num',
          'order'   => 'ASC',
        ];

        // Maybe it's multilingual
        if (function_exists('pll_current_language')) {
            $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
        }

        // Custom query.
        $myquery = new WP_Query($args);
        // Check that we have query results.
        if ($myquery->have_posts()) {
            // Start looping over the query results.
            while ($myquery->have_posts()) {
                $myquery->the_post();
                ?>
                <div class="col-lg-6 col-12 pl-lg-0 text-center secretariat-content">
                    <img src="<?= get_field('image') ?>" />
                </div>
                <?php
            }
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>
    </div>

</div>