<?php

$blockslug = 'secretariat';

// Start the block
$block             = [];
$block['title']    = 'Secretariat';
$block['icon']     = 'dashicons dashicons-calendar';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Add the controls to the block
$block['controls'] = $controls;
$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__.'/'. $blockslug . '-template.php');
$code['use_php']       = true;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
