<style scoped>

    @media screen and (min-width: 992px) {
        .on-the-fly-{{#blockid}} {
            {{#if customposition}}
            position: relative;
            top: {{custompositiontop}}px;
            left: {{custompositionleft}}px;
            {{/if}}
            {{#if custompadding}}
            padding-top: {{custompaddingtop}}px;
            padding-left: {{custompaddingleft}}px;
            padding-bottom: {{custompaddingbottom}}px;
            padding-right: {{custompaddingright}}px;
            {{/if}}
            {{#if custommargin}}
            margin-top: {{custommargintop}}px;
            margin-left: {{custommarginleft}}px;
            margin-bottom: {{custommarginbottom}}px;
            margin-right: {{custommarginright}}px;
            {{/if}}
            {{#if customwidth}}
            width: {{customwidth}}%;
            {{/if}}
        }
    }

</style>
