<div class="container-md">
    <div class="row lazyblock-latest-news-events">
        <?php
        $args = [
          'post_type'      => 'news_item',
          'posts_per_page' => 2,
          'order'          => 'DESC',
          'orderby'        => 'publish_date',
        ];

        // Maybe it's multilingual
        if (function_exists('pll_current_language')) {
            $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
        }

        // Custom query.
        $myquery = new WP_Query($args);
        // Check that we have query results.
        if ($myquery->have_posts()) {
            ?>
            <div class="col-12 col-lg-6 p-lg-4 latest-news">
                <div class="row">
                    <div class="col-12 pl-lg-4 latest-news-intro">
                        <h2>Latest <span>News</span></h2>
                    </div>
                </div>
                <div class="latest-news-wrapper row">
                    <?php
                    // Start looping over the query results.
                    while ($myquery->have_posts()) {
                        $myquery->the_post();
                        $title = the_title('','',false);
                        ?>
                        <div class="news-item-wrapper col-lg-6 col-12 p-4">
                            <a href="<?= the_permalink() ?>">
                                <div class="news-item-inner">
                                    <?= the_post_thumbnail() ?>
                                    <div class="news-item-title" title="<?= $title; ?>"><?= project_truncate_words($title, 70); ?></div>
                                </div>

                            </a>
                        </div>

                        <?php
                    }
                    ?>
                    <div class="col-12 pl-lg-4 pl-4 view-all">
                        <a href="/news" class="btn btn-blue">View all news</a>
                    </div>
                </div>
            </div>
            <?php
        }
        $args = [
          'post_type'      => 'event',
          'posts_per_page' => 1,
          'order'          => 'DESC',
          'orderby'        => 'publish_date',
        ];

        // Maybe it's multilingual
        if (function_exists('pll_current_language')) {
            $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
        }

        // Custom query.
        $myquery = new WP_Query($args);
        // Check that we have query results.
        if ($myquery->have_posts()) {
            ?>
            <div class="col-12 col-lg-6 p-lg-4 latest-events">
                <div class="row">
                    <div class="col-12 pl-lg-4 latest-events-intro">
                        <h2>Next <span>Event</span></h2>
                    </div>
                </div>
                <div class="row latest-event-wrapper">
                    <?php
                    // Start looping over the query results.
                    while ($myquery->have_posts()) {
                        $myquery->the_post();
                        ?>
                        <div class="col-lg-8 col-12 p-lg-4 pt-4 pl-4 pr-4 events-item-wrapper">
                            <a href="<?= the_permalink() ?>">
                                <div class="events-item-inner">
                                    <?= the_post_thumbnail() ?>
                                </div>

                            </a>
                        </div>
                        <div class="col-lg-4 col-12 pt-2 pt-lg-4 pl-lg-0 pl-4 events-item-wrapper">
                            <div class="events-item-inner">
                                <div class="event-date pt-3">
                                    <i class="far fa-calendar-alt"></i> <?= get_field('starting_date') ?>
                                </div>
                                <div class="event-time pt-3">
                                    <i class="far fa-clock"></i> <?= get_field('starting_time') ?>
                                </div>
                                <div class="event-register pt-3">
                                    <a href="<?= the_permalink() ?>">Discover more</a>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                    ?>
                    <div class="col-12 pl-lg-4 pl-4 pt-lg-0 pt-4 view-all">
                        <a href="/events" class="btn btn-blue">View all events</a>
                    </div>
                </div>
            </div>
            <?php
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>
    </div>

</div>