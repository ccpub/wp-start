<div class="container-md">
    <div class="row lazyblock-meps justify-content-lg-between">
            <?php
            $args = [
              'post_type'      => 'mep',
              'posts_per_page' => -1,
              'order'          => 'DESC',
              'orderby'        => 'publish_date',
            ];

            // Maybe it's multilingual
            if (function_exists('pll_current_language')) {
                $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
            }

            // Custom query.
            $myquery = new WP_Query($args);
            // Check that we have query results.
            if ($myquery->have_posts()) {
                // Start looping over the query results.
                while ($myquery->have_posts()) {
                    $myquery->the_post();
                    ?>
                    <div class="col">
                            <div class="card px-0">
                                <div class="card-image text-center">
                                    <img src="<?php the_field('image'); ?>" />
                                </div>
                                <div class="card-name text-center"><b><?= the_title(); ?></b></div>
                                <div class="card-political-group text-center"><?= get_field('political_group') ?></div>
                                <div class="card-country text-center"><?= get_field('country') ?></div>
                                <?php if (get_field('link')): ?>
                                <div class="card-link text-center"><a href="<?= get_field('link') ?>" target="_blank" class="mt-2"><i class="fas fa-link"></i></a></div>
                                <?php endif; ?>
                            </div>
                    </div>

                    <?php
                }
            }
            // Restore original post data.
            wp_reset_postdata();
            ?>
        </div>
</div>