<span>
{{#include 'image-customs-style'}}
{{#if fullwidth}}
    <div class="img-wrapper">
        <img class="lazyblock-image-fullwidth on-the-fly-{{#blockid}}" src="{{image.url}}" alt="{{image.alt}}"
         title="{{image.alt}}"/>
    </div>
        {{else}}
<div class="row lazyblock-image">
    <div class="col-lg-12 d-flex {{alignment}}">
        <img class="on-the-fly-{{#blockid}}" src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
    </div>
</div>
{{/if}}
</span>
