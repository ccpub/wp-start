<?php

// Custom
$control                         = [];
$control['label']                = 'Custom Position';
$control['name']                 = 'customposition';
$control['type']                 = 'toggle';
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-position';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Top';
$control['name']                 = 'custompositiontop';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-position-top';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Left';
$control['name']                 = 'custompositionleft';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-position-left';

// Add the control to the controls
$controls[$control_id] = $control;


// Custom
$control                         = [];
$control['label']                = 'Custom Padding';
$control['name']                 = 'custompadding';
$control['type']                 = 'toggle';
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-padding';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Top';
$control['name']                 = 'custompaddingtop';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-padding-top';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Left';
$control['name']                 = 'custompaddingleft';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-padding-left';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Bottom';
$control['name']                 = 'custompaddingbottom';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-padding-bottom';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Right';
$control['name']                 = 'custompaddingright';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-padding-right';

// Add the control to the controls
$controls[$control_id] = $control;



// Custom
$control                         = [];
$control['label']                = 'Custom Margin';
$control['name']                 = 'custommargin';
$control['type']                 = 'toggle';
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-margin';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Top';
$control['name']                 = 'custommargintop';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-margin-top';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Left';
$control['name']                 = 'custommarginleft';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-margin-left';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Bottom';
$control['name']                 = 'custommarginbottom';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-margin-bottom';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Right';
$control['name']                 = 'custommarginright';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-1000';
$control['max']                  = '1000';
$control['step']                 = '10';
$control['default']              = '50';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-margin-right';

// Add the control to the controls
$controls[$control_id] = $control;

// Custom
$control                         = [];
$control['label']                = 'Custom Width';
$control['name']                 = 'customwidth';
$control['type']                 = 'toggle';
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-width';

// Add the control to the controls
$controls[$control_id] = $control;

// Number
$control                         = [];
$control['label']                = 'Width %';
$control['name']                 = 'customwidth';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '0';
$control['max']                  = '100';
$control['step']                 = '10';
$control['default']              = '100';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-customs-custom-width-width';

// Add the control to the controls
$controls[$control_id] = $control;
