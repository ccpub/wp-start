<div class="row lazyblock-cta">
    <div class="col-lg-12 text-{{alignment}}">
        &nbsp;<a class="btn {{buttonstyle}}" href="{{url}}" target="{{target}}">{{label}}</a>
    </div>
</div>
