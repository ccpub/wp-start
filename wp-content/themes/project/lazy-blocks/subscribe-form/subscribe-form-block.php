<?php

$blockslug = 'subscribe-form';

// Start the block
$block             = [];
$block['title']    = 'Subscribe';
$block['icon']     = 'dashicons dashicons-controls-play';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Title
$control             = [];
$control['label']    = 'Title';
$control['name']     = 'title';
$control['type']     = 'text';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-title';

// Add the control to the controls
$controls[$control_id] = $control;

// Title
$control             = [];
$control['label']    = 'Subtitle';
$control['name']     = 'subtitle';
$control['type']     = 'text';
$control['child_of'] = '';

// Make an id.
$control_id = 'control-subtitle';

// Add the control to the controls
$controls[$control_id] = $control;

// Button label
$control             = [];
$control['label']    = 'Button label';
$control['name']     = 'button_label';
$control['type']     = 'text';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-button-label';

// Add the control to the controls
$controls[$control_id] = $control;

// URL
$control             = [];
$control['label']    = 'Url';
$control['name']     = 'url';
$control['type']     = 'url';
$control['child_of'] = '';

// Make an id.
$control_id = 'control-cta-url';

// Add the control to the controls
$controls[$control_id] = $control;

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = true;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
