<div class="row lazyblock-subscribe-form pt-4 pb-4">
    <div class="col-lg-12 text-center">
        <h2><?= $attributes['title'] ?></h2>
        <h3><?= $attributes['subtitle'] ?></h3>
        <a href="<?= $attributes['url'] ?>" class="btn btn-blue mt-3 mb-3"><?= $attributes['button_label'] ?></a>
    </div>
</div>