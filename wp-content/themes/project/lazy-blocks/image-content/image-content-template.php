<span>
{{#include 'image-customs-style'}}
<div class="row lazyblock-image-content">
    <div class="col-lg-{{division}}">
        <img class="on-the-fly-{{#blockid}}" src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}" />
    </div>
    <div class="col-lg-{{#leftover division}}">
        {{{content}}}
    </div>
</div>
</span>