<div class="container-md">
    <div class="row lazyblock-news-items">
        <?php
        $args = [
          'post_type'      => 'news',
          'posts_per_page' => (int) $attributes['amount_of_news_items'],
          'order'          => 'DESC',
          'orderby'        => 'publish_date',
        ];

        // Maybe it's multilingual
        if (function_exists('pll_current_language')) {
            $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
        }

        // Custom query.
        $myquery = new WP_Query($args);
        // Check that we have query results.
        if ($myquery->have_posts()) {
            // Start looping over the query results.
            while ($myquery->have_posts()) {
                $myquery->the_post();
                ?>
                <div class="col-12 p-4 news-item-wrapper">
                    <a href="<?= the_permalink() ?>">
                        <div class="row">
                            <div class="col-lg-4 col-12">
                                <?= the_post_thumbnail() ?>
                            </div>
                            <div class="col-lg-8 col-12 mt-4 mt-lg-0">
                                <div class="news-item-title"><h3><?= the_title(); ?></h3></div>
                                <div class="news-item-date"><?= the_date() ?></div>
                                <div class="news-item-summary mt-4">
                                    <?= !empty(get_field('summary')) ? get_field('summary') : create_fallback_string(get_the_content()); ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <?php
            }

            the_posts_pagination(array(
                                     'screen_reader_text' => __('Pages'),
                                     'mid_size' => 1,
                                     'prev_text' => __('Previous'),
                                     'next_text' => __('Next'),
                                 ));
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>
    </div>

</div>