<div class="container-fluid lazyblock-section-{{background}}">
<div class="container-md lazyblock-section">
  <div class="row no-gutters">

    {{#ifEquals squeeze '0'}}
    <div class="col-lg-12">{{{content}}}</div>
    {{/ifEquals}}

    {{#ifNotEquals squeeze '0'}}
    <div class="col-lg-{{#halfleftover squeeze}}"></div>
    <div class="col-lg-{{squeeze}}">{{{content}}}</div>
    <div class="col-lg-{{#halfleftover squeeze}}"></div>
    {{/ifNotEquals}}

  </div>
</div>
</div>
