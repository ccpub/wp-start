<div class="row lazyblock-form">
    <div class="container-md">
        <div class="row">
            <div class="col-lg-12">
                <h2><?= $attributes['label'] ?></h2>
                <?php
                require get_template_directory() . '/forms/' . $attributes['form'] . '-form.php';
                ?>
            </div>
        </div>
    </div>
</div>