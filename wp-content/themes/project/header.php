<!doctype html>
<html lang="<?= str_replace("_", "-", bloginfo('language')); ?>" class="load-hidden">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php wp_head(); ?>

</head>
<body>
    <div class="navbar-container fixed-top">
        <nav class="navbar navbar-light navbar-expand-lg">
            <a class="navbar-brand" href="<?= bloginfo('url'); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/logo-tagline.svg"
                     alt="Logo" class="d-inline-block align-top"/>
            </a>
            <div class="tagline">
                <a href="<?= bloginfo('url'); ?>">
                    <?= bloginfo('description'); ?>
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse justify-content-lg-end" id="navbarNav">
                <ul id="mainmenu" class="navbar-nav">
                    <?php wp_nav_menu([
                      'theme_location' => 'main-menu',
                      'menu_class' => 'col nav navbar-nav ml-auto',
                      'container_id' => 'navbarNav',
                      'container_class' => 'collapse navbar-collapse',
                      'after' => '<span class="menu-separator"></span>',
                      'bootstrap' => true,
                      'walker' => new \Project\BootstrapNavMenuWalker(),
                    ]); ?>
                    <li class="twitter-item">
                        <a href="https://twitter.com/SLICEI_IG" target="_blank"><i class="dashicons dashicons-twitter"></i><span class="d-lg-none"> Follow us</span></a>
                    </li>
                    <li class="search-form-item"><?php get_search_form( ); ?></li>

                </ul>


            </div>

        </nav>
    </div>
<main>
<?php
if (get_field('header_image')) {
    ?>
    <div class="container-fluid px-0 header-image">
        <img src="<?= get_field('header_image')['url'] ?>" />
    </div>
    <?php
}
?>
