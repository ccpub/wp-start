<form id="newsletter-form" action="">
    <input type="hidden" name="form_id" value="newsletter">
    <input type="hidden" name="locale" value="<?= get_locale() ?>">
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="first_name"><?= pll__('first_name') ?> *</label>
                <input type="text" class="form-control" name="first_name"
                       id="first_name" required>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="last_name"><?= pll__('last_name') ?> *</label>
                <input type="text" class="form-control" name="last_name"
                       id="last_name" required>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="organisation"><?= pll__('organisation') ?> *</label>
                <input type="text" class="form-control" name="organisation"
                       id="organisation" required>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="job_title"><?= pll__('job_title') ?> *</label>
                <input type="text" class="form-control" name="job_title"
                       id="job_title" required>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="telephone_number"><?= pll__('telephone_number') ?> *</label>
                <input type="text" class="form-control" name="telephone_number"
                       id="telephone_number" required>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="email"><?= pll__('email') ?> *</label>
                <input type="email" class="form-control" name="email" id="email" required>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="form-group">
                <div class="form-check">
                    <input name="newsletter" type="hidden" value="0">
                    <input class="form-check-input" name="newsletter" value="1" type="checkbox" id="newsletter">
                    <label class="form-check-label" for="newsletter">
                        <?= pll__('newsletter_label') ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-12">
            <div class="form-group">
                <div class="form-check">
                    <label class="form-check-label">
                        * <?= pll__('required_fields') ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary"><?= pll__('subscribe') ?></button>

        </div>
    </div>
</form>
<div id="newsletter-thankyou" class="d-none">
    <h3><?= pll__('newsletter-thankyou-title') ?></h3>
    <p>
        <?= pll__('newsletter-thankyou-text') ?>
    </p>
</div>
<script>
  jQuery(document).ready(function ($) {
    // Initialize InputMask
    $(":input").inputmask();
  });

  function grayer(formId, yesNo) {
    var f = document.getElementById(formId), s, opacity;
    s = f.style;
    opacity = yesNo ? '40' : '100';
    s.opacity = s.MozOpacity = s.KhtmlOpacity = opacity / 100;
    s.filter = 'alpha(opacity=' + opacity + ')';
    for (var i = 0; i < f.length; i++) {
      f[i].disabled = yesNo;
    }
  }

  // Submit a form using JS not the traditional way ;)
  // Insteqd of click you will use submit
  jQuery('#newsletter-form').on('submit', function (e) {

    // Don't actually submit the form.
    e.preventDefault();

    // Gather the data in the form.
    // For real you probably will user jQuery('#testform').serialize()
    var data = jQuery('#newsletter-form').serialize()

    // This would be a great place to hide your form and show a spinner
    grayer('newsletter-form', true);

    // Execute the google recaptcha browser part
    grecaptcha.execute('<?= ccfigValue('recaptcha-public-token')?>', {action: 'newsletter'}).then(function (token) {

      // Add the token response to the data of the form.
      data = data + '&token=' + token;
      console.log(data);
      data = data.replace("#038;", "", data);
      console.log(data);


      // Post the data of the form to ccforms plugin.
      jQuery.post('/wp-json/ccforms/v1/form', data, function (response) {

        // This should be 1 of 2 things: OK or NOK
        //console.log(response);

        // This would be a great place to stop showing your spinner and instead show a thank you.
        // Or error message...
        console.log(response);
        if (response == 'OK') {
          jQuery('#newsletter-form').addClass('d-none');
          jQuery('#newsletter-thankyou').removeClass('d-none');
        }
        else {
          grayer('newsletter-form', false);
        }

      });
    });
  });

</script>