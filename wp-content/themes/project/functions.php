<?php

// Add the title tag to the wp_head()
add_theme_support('title-tag');
add_theme_support('post-thumbnails', ['news', 'event']);

// Include stuff to help us out.
include_once __DIR__ . '/includes/scripts-and-styles.php';
include_once __DIR__ . '/includes/breadcrumbs.php';
include_once __DIR__ . '/includes/helpers.php';
include_once __DIR__ . '/includes/menus.php';
include_once __DIR__ . '/includes/shortcodes.php';
include_once __DIR__ . '/includes/lazyblocks.php';
include_once __DIR__ . '/includes/handlebars-helpers.php';
include_once __DIR__ . '/includes/ccforms.php';
include_once __DIR__ . '/includes/editor.php';
include_once __DIR__ . '/includes/blog.php';
include_once __DIR__ . '/includes/BootstrapNavMenuWalker.php';


/////////// CUSTOM STUFF GOES BELOW ///////////

// Please, when you see that some functions or code is grouped,
// put that into an include.
