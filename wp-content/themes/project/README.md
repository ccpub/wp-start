# Wordpress Project Theme

Extremely simple wordpress theme to get your started with bootstrap, fontawesome, country flags, and many nice lazyblocks.

You should have lazyblocks installed in your project.

```
wp plugin install lazy-blocks --activate
```


## Install

```
wget https://bitbucket.org/contentncoffee/wordpress-project-theme/get/master.zip
```

Download and copy the inner contents to a directory called "project" in your themes folder.

or

Copy this into an existing folder and edit the style.css to match the name.

## Contribute

Clonse this, make changes, and make a pull request.

## Lazy Blocks

There are a number of lazy blocks being made in this theme.

If you want to make more do so the same way the others are made by using code.

Using the Lazy Blocks admin and then exporting is useful for learning but ultimately all content should be based on blocks made in code.
