<?php
/**
 * @file In this file add anything that changes how "editing" works in the admin.
 */


//////// TINY MCE MODS ////////
// Limit the formats to P H3 and H4
function project_limit_formats($arr)
{
    $arr['block_formats'] = 'Paragraph=p;H2=h2;H3=h3;H4=h4';
    return $arr;
}

add_filter('tiny_mce_before_init', 'project_limit_formats');

/**
 * Add our plugin to the tinymce.
 * @param $plugins
 *
 * @return mixed
 */
function project_tiny_mce_add_buttons($plugins)
{
    $plugins['mytinymceplugin'] = get_template_directory_uri() . '/js/tiny-mce/tiny-mce.js';
    return $plugins;
}

/**
 * Register our new buttons in the tinymce.
 * @param $buttons
 *
 * @return array
 */
function project_tiny_mce_register_buttons($buttons)
{
    $newBtns = [
    'myctabtn',
    'mywhoswhobtn',
    'myimpressivebtn',
    'mygoimgbtn',
    'mypoibtn',
    ];
    $buttons = array_merge($buttons, $newBtns);
    return $buttons;
}

/**
 * Register the buttons and plugin on init.
 */
function project_tiny_mce_new_buttons()
{
    add_filter('mce_external_plugins', 'project_tiny_mce_add_buttons');
    add_filter('mce_buttons', 'project_tiny_mce_register_buttons');
}

// Don't remove this.
//add_action('init', 'project_tiny_mce_new_buttons');


/**
 * Take away stuff from the Gutenberg editor that we don't want people doing.
 * Like, font color, size, ...
 */
function project_gutenberg_remove_text_design()
{
    echo '<style>
     .blocks-font-size { display: none; }
     .editor-block-inspector__advanced { display: none; }
     .block-editor-panel-color-settings { display: none; }
     </style>';
}
// Don't remove this.
add_action('admin_head', 'project_gutenberg_remove_text_design');


/**
 * Enqueue WordPress theme styles within Gutenberg.
 */
function project_gutenberg_styles()
{
    wp_enqueue_style('project-gutenberg', get_theme_file_uri('/gutenberg.css'), false, '1.0', 'all');
    wp_enqueue_script('project-gutenberg-js', get_theme_file_uri('/gutenberg.js'), false, '1.0', false);
}

add_action('enqueue_block_editor_assets', 'project_gutenberg_styles');
