<?php
/**
 * @file Regsiter the scripts and styles for the project.
 */

/**
 * Register the scripts and styles for the project front end.
 */
function project_add_scripts_and_styles()
{
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/css/custom.css', ['bootstrap']);
    wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css');
    wp_enqueue_style('inputmask', 'https://cdn.jsdelivr.net/gh/RobinHerbots/jquery.inputmask@5.0.0-beta.87/css/inputmask.css');
    wp_enqueue_style('dashicons');

    wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.5.1.slim.min.js', [], false, true);
    wp_enqueue_script('jqueryinputmask', 'https://cdn.jsdelivr.net/gh/RobinHerbots/jquery.inputmask@5.0.0-beta.87/dist/jquery.inputmask.min.js', [], false, true);
    wp_enqueue_script('popper', 'https://unpkg.com/@popperjs/core@2', [], false, true);
    wp_enqueue_script(
        'bootstrap',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js',
        [],
        false,
        true
    );
    wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/321e9b3bc8.js', [], false, true);
    wp_enqueue_script('project', get_template_directory_uri() . '/js/project.js', [], false, true);
    wp_enqueue_script(
        'recaptcha',
        'https://www.google.com/recaptcha/api.js?render=' . ccfigValue('recaptcha-public-token'),
        [],
        false,
        true
    );

  // Called so that it is registered.
    $temp = ccfigValue('recaptcha-secret-token');
}

add_action('wp_enqueue_scripts', 'project_add_scripts_and_styles');

/**
 * Add scripts and styles just in the admin.
 */
function project_admin_enqueue_scripts()
{
}

add_action('admin_enqueue_scripts', 'project_admin_enqueue_scripts');
