<?php
/**
 * @file In this file we do all the stuffs with CCFORMS.
 */

// Example of adding headers and fields to the export and form data collection.
// Examples on how to use the filters with the plugin.

add_filter('ccforms_form_entries_headers', 'project_export_form_headers', 10, 2);
add_filter('ccforms_form_entries_fields', 'project_export_form_fields', 10, 2);
add_filter('ccforms_form_ids', 'project_form_ids', 10, 1);
add_filter('ccforms_sends', 'project_form_sends', 10, 1);
add_filter('ccforms_do_not_store_in_db', 'project_do_not_store_in_db', 10, 1);
add_filter('ccforms_to', 'project_form_to', 10, 2);
add_filter('ccforms_recaptcha_secret_token', 'project_form_recaptcha', 10, 1);

/**
 * Define what the headers need to be in the export CSV
 *
 * @param $headers
 * @param $form_id
 *
 * @return array
 */
function project_export_form_headers($headers, $form_id)
{
    if ($form_id == 'contact') {
        $headers[] = 'First Name';
        $headers[] = 'Last Name';
        $headers[] = 'Message';
    }

    if ($form_id == 'newsletter') {
        $headers[] = 'First name';
        $headers[] = 'Last name';
    }

    if ($form_id == 'demo') {
        $headers[] = 'Company';
        $headers[] = 'Files';
        $headers[] = 'Software';
        $headers[] = 'First Name';
        $headers[] = 'Last Name';
        $headers[] = 'Phone';
    }

    return $headers;
}

/**
 * Define the fields that need to gathered and exported for that form id.
 *
 * @param $fields
 * @param $form_id
 *
 * @return array
 */
function project_export_form_fields($fields, $form_id)
{
    if ($form_id == 'contact') {
        $fields[] = 'firstname';
        $fields[] = 'lastname';
        $fields[] = 'message';
    }

    if ($form_id == 'newsletter') {
        $fields[] = 'first_name';
        $fields[] = 'last_name';
    }

    if ($form_id == 'demo') {
        $fields[] = 'company';
        $fields[] = 'files';
        $fields[] = 'software';
        $fields[] = 'firstname';
        $fields[] = 'lastname';
        $fields[] = 'phone';
    }

    return $fields;
}

/**
 * Which form ids are we are going to work on.
 *
 * @param $form_ids
 *
 * @return array
 */
function project_form_ids($form_ids)
{
    $form_ids[] = 'contact';
    $form_ids[] = 'newsletter';
    $form_ids[] = 'demo';

    return $form_ids;
}

/**
 * Which forms send email.
 *
 * @param $form_ids
 *
 * @return array
 */
function project_form_sends($form_ids)
{
    $form_ids[] = 'contact';
    $form_ids[] = 'newsletter';
    $form_ids[] = 'demo';

    return $form_ids;
}

function project_do_not_store_in_db($form_ids) {
    $form_ids[] = 'contact';

    return $form_ids;
}

/**
 * Which email to send to on which form.
 *
 * @param $to
 * @param $form_id
 *
 * @return array|mixed|object|null
 */
function project_form_to($to, $form_id)
{
    if ($form_id == 'contact') {
        $to = ccfigValue('contact-form-email-to');
    }
    if ($form_id == 'newsletter') {
        $to = ccfigValue('newsletter-form-email-to');
    }
    if ($form_id == 'demo') {
        $to = ccfigValue('demo-form-email-to');
    }

    return $to;
}

/**
 * The secret token to use for verifying humans
 *
 * @param $secretKey
 *
 * @return array|mixed|object|null
 */
function project_form_recaptcha($secretKey)
{
    return ccfigValue('recaptcha-secret-token');
}


add_filter('ccevents_recaptcha_secret_token', 'project_form_recaptcha', 10, 1);
