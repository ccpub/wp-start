<?php

$seconds_to_cache = 300;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");
get_header();
if (have_posts()) {
  // Load posts loop.
    while (have_posts()) {
        the_post();
        ?>
    <div class="container-md">
      <div class="row no-gutters posts-wrapper">
        <div class="col-lg-12">
          <h1><?php the_title(); ?></h1>
          <?php
            $featured_image = get_the_post_thumbnail_url(null, 'full');
            if ($featured_image) {
                ?>
            <div class="img-wrapper">
              <img src="<?= $featured_image ?>" class="figure-img img-fluid" alt="<?= the_title(); ?>">
            </div>
            <?php } ?>
          <div class="mb-5"><p><?php echo strtolower(get_the_date('d M Y')); ?></p></div>
            <?php
            the_content();
            ?>

          <div class="row mt-5">
            <?php
            $next = get_next_post();
            $prev = get_previous_post();
            ?>

            <?php if ($prev) { ?>
              <div class="col align-self-start text-left">
                <a href="<?= get_permalink($prev) ?>" class="blog-nav-link"><i
                    class="fas fa-chevron-left pr-1"></i> <?= $prev->post_title ?></a>
              </div>
            <?php } ?>

            <?php if ($next) { ?>
              <div class="col align-self-end text-right">
                <a href="<?= get_permalink($next) ?>" class="blog-nav-link"><?= $next->post_title ?> <i
                    class="fas fa-chevron-right pl-1"></i></a>
              </div>
            <?php } ?>

          </div>
        </div>
      </div>
    </div>
        <?php
    }
} else {
    get_404_template();
}

get_footer();
