<?php
//to override this template, create a file named hmsclassic.php within your active theme folder. you should probably copy and paste this file's code as a starting point.
?>
<?php if ($mobile_friendly) : ?>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
<?php endif ?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link href="<?= get_template_directory_uri()?>/css/custom.css" rel="stylesheet" >
<body>
<?php echo $messagehtml ?>
<div class="container">
    <div class="row justify-content-center" style="min-height: 100vh">
        <div class="col-12 col-lg-4 col-md-6 d-flex wrap text-center">
            <div id='form_wrap' class="form-group flex-centered w-100">
                <form method=post class="w-100">
                    <input type=password name='hwsp_motech' placeholder='Password' class='form-control enter_password mb-4'>
                    <?php echo $hinthtml ?>
                    <button type=submit class="btn btn-primary mt-4">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
