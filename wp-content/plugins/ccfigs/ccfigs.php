<?php
/*
Plugin Name: CC Figs
Description: Creates a post type and provides functionality for creating and managing configurations.
Version:     1.0.0
Author:      CC
*/


// Helper functions.
require_once dirname(__FILE__) . '/helpers.php';

/**
 * Register the config type
 */
function ccfigs_register_config()
{
    register_post_type(
        'config',
        [
            'labels'      => [
                'name'          => 'Configs',
                'singular_name' => 'Config',
                'menu_name' => 'Configs',
                'all_items' => 'All Configs',
                'edit_item' => 'Edit Config',
                'search_items' => 'Search Configs',
            ],
            'public'      => false,
            'has_archive' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'supports'           => ['title'],
            'capability_type' => 'post',
            'capabilities' => array(
                'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
            ),
            'map_meta_cap' => true,
        ]
    );
}
add_action('init', 'ccfigs_register_config');
add_filter('pll_get_post_types', 'ccfigs_add_configs_to_pll', 10, 2);
function ccfigs_add_configs_to_pll($post_types, $is_settings)
{
    $post_types['config'] = 'config';
    return $post_types;
}

/**
 * Add the field groups to configs.
 * This requires advance custom fields.
 */
if (function_exists('acf_add_local_field_group')) :
    acf_add_local_field_group(array(
        'key' => 'group_5d84bf3108ead',
        'title' => 'Config Values',
        'fields' => array(
            array(
                'key' => 'field_5d84bf3bc41d3',
                'label' => 'Type',
                'name' => 'type',
                'type' => 'radio',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'text' => 'Text',
                    'content' => 'Content',
                    'bool' => 'Bool',
                    'media' => 'Media',
                    'json' => 'JSON',
                    'post' => 'Post',
                ),
                'allow_null' => 0,
                'other_choice' => 0,
                'default_value' => 'text',
                'layout' => 'horizontal',
                'return_format' => 'value',
                'save_other_choice' => 0,
            ),
            array(
                'key' => 'field_5d84bfa34b622',
                'label' => 'Text Value',
                'name' => 'text_value',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5d84bf3bc41d3',
                            'operator' => '==',
                            'value' => 'text',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d84bfcb4b623',
                'label' => 'Content Value',
                'name' => 'content_value',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5d84bf3bc41d3',
                            'operator' => '==',
                            'value' => 'content',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'visual',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5d95a44019c32',
                'label' => 'Bool Value',
                'name' => 'bool_value',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5d84bf3bc41d3',
                            'operator' => '==',
                            'value' => 'bool',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 0,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
            array(
                'key' => 'field_5d84bffc4b624',
                'label' => 'Media Value',
                'name' => 'media_value',
                'type' => 'file',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5d84bf3bc41d3',
                            'operator' => '==',
                            'value' => 'media',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
                'library' => 'all',
                'min_size' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => 'field_5d84c15210dce',
                'label' => 'JSON Value',
                'name' => 'json_value',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5d84bf3bc41d3',
                            'operator' => '==',
                            'value' => 'json',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ),
            array(
                'key' => 'field_5d8874df26833',
                'label' => 'Post Value',
                'name' => 'post_value',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5d84bf3bc41d3',
                            'operator' => '==',
                            'value' => 'post',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => '',
                'taxonomy' => '',
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'id',
                'ui' => 1,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'config',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
            0 => 'permalink',
            1 => 'the_content',
            2 => 'excerpt',
            3 => 'discussion',
            4 => 'comments',
            5 => 'revisions',
            6 => 'slug',
            7 => 'author',
            8 => 'format',
            9 => 'page_attributes',
            10 => 'featured_image',
            11 => 'categories',
            12 => 'tags',
            13 => 'send-trackbacks',
        ),
        'active' => true,
        'description' => '',
    ));
endif;

/**
 * Get a config value.
 *
 * @param $slug The slug of the config.
 *
 * @param null $default Send this value back if the config value is not returned.
 *
 * @return array|mixed|object|null
 */
function ccfigValue($slug, $default = null)
{

    // Force slugged title.
    $slug = sanitize_title($slug);

    $args              = [];
    $args['post_type'] = 'config';
    $args['title']     = $slug;

    // Maybe it's multilingual
    if (function_exists('pll_current_language')) {
        $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
    }

    // Get the one post that matches.
    $myquery = new WP_Query($args);

    // There should be one and only one if there are more we are in trouble.
    if ($myquery->post_count == 1) {
        $post = $myquery->post;
        $type = get_field('type', $post->ID);

        // At present these are the values / types.
        $keys            = [];
        $keys['text']    = 'text_value';
        $keys['content'] = 'content_value';
        $keys['bool']    = 'bool_value';
        $keys['media']   = 'media_value';
        $keys['json']    = 'json_value';
        $keys['post']    = 'post_value';


        // Make sure the type is correct, and get that value.
        if (isset($keys[ $type ])) {
            $value = get_field($keys[ $type ], $post->ID);

            // If it was a json value
            if ($type == 'json') {
                $value = json_decode($value);
            }
            // If it was a bool value
            if ($type == 'bool') {
                $value = $value ? true : false;
            }

            return $value;
        }
    }

    // It does not exist, let's create.
    if ($myquery->post_count == 0) {
        // Now create a the config is all languages.
        $entry                             = [];
        $entry['post_title']               = $slug;
        $entry['post_type']                = 'config';
        $entry['post_status']              = 'publish';
        $entry['meta_input']               = [];
        $entry['meta_input']['type']       = 'text';
        $entry['meta_input']['text_value'] = $default;

        // If we have multilingual turned on make many.
        if (function_exists('pll_languages_list')) {
            // Get all active languages
            $languages = pll_languages_list([ 'hide_empty' => false, ]);

            // Create the posts than store langs and ids
            $postids = [];
            foreach ($languages as $lang) {
                $postids[ $lang ] = wp_insert_post($entry);
            }
            foreach ($postids as $lang => $id) {
                pll_set_post_language($id, $lang);
            }
            // Bind posts with translations
            pll_save_post_translations($postids);
        } else {
            // Or just make one.
            wp_insert_post($entry);
        }
    }

    return $default;
}

/**
 * Modify the title on insert to make sure it is a slug.
 */
add_filter('wp_insert_post_data', 'ccfig_title_modify_on_insert', '99', 2);
function ccfig_title_modify_on_insert($data, $postarr)
{
    if ($data['post_type'] == 'config') {
        $data['post_title'] = sanitize_title($data['post_title']);
    }
    return $data;
}

/**
 * modify the title on update to make sure it is a slug.
 */
add_action('pre_post_update', 'ccfig_title_modify_on_update');
function ccfig_title_modify_on_update($post_id)
{
    $type = get_post_type($post_id);
    if ($type == 'config') {
        $post             = get_post($post_id);
        $post->post_title = sanitize_title($post->post_title);
    }
}
