<?php

/**
 * Get the distinct values in a meta field.
 * TODO: add a type argument to this function for more interesting results.
 *
 * @param $type
 * @param $key
 *
 * @return array
 */
function _get_all_meta_values($type, $key)
{
    /** @var \wpdb $wpdb */
    global $wpdb;
    $result = $wpdb->get_col(
        $wpdb->prepare(
            "
			SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
			LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
			WHERE pm.meta_key = '%s' 
			AND p.post_status = 'publish'
			AND p.post_type = '%s'
			ORDER BY pm.meta_value",
            $key,
            $type
        )
    );

    return $result;
}

/**
 * Return the nice string from a machine name.
 * something_like_this -> Something Like This
 *
 * @param $machine
 *
 * @return string
 */
function nice_name($machine)
{
    return ucwords(strtolower(str_replace("_", " ", $machine)));
}

/**
 * Most of the isabel users use windows so...
 * @param $array
 * @return mixed
 */
function convertToISOCharset($array)
{
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $array[$key] = convertToISOCharset($value);
        } else {
            $array[$key] = mb_convert_encoding($value, 'ISO-8859-1', 'UTF-8');
        }
    }
    return $array;
}
