# Install plugins

### INSTALL PLUGINS FOR LOCAL DEV ###
if [[ $1 == 'local' ]]; then
wp plugin install polylang --activate --force --allow-root
wp plugin install lazy-blocks --activate --force --allow-root
wp plugin install advanced-custom-fields --activate --force --allow-root
wp plugin install custom-post-type-ui --activate --force --allow-root
wp plugin install disable-comments --activate --force --allow-root
wp plugin install redirection --activate --force --allow-root
wp plugin install taxonomy-terms-order --activate --force --allow-root
wp plugin install theme-translation-for-polylang --activate --force --allow-root
wp plugin install tinymce-advanced --activate --force --allow-root
wp plugin install wp-migrate-db --activate --force --allow-root
wp plugin install autodescription --activate --force --allow-root
wp plugin install reset-meta-box-positions --activate --force --allow-root
fi


### INSTALL PLUGINS FOR THE PROJECT ###
#wp plugin install autodescription --activate --force --allow-root
